class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates_presence_of( :title, :message => "No title")
  validates_presence_of( :body, :message => "No Body")
end
